import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {AwardComponent} from "./award.component";
import {ComponentsModule} from "../../components/components.module";

describe("AwardComponent", () => {
  let component: AwardComponent;
  let fixture: ComponentFixture<AwardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AwardComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
