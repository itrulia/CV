import {Component, Input, ChangeDetectionStrategy} from "@angular/core";

export interface Award {
  timeframe: string;
  competition: string;
  placement: string;
}

@Component({
  selector: "cv-award",
  templateUrl: "./award.component.html",
  styleUrls: ["./award.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AwardComponent {
  @Input()
  public timeframe: string;

  @Input()
  public placement: string;
}
