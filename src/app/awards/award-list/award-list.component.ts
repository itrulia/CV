import {Component, ChangeDetectionStrategy} from "@angular/core";
import {Award} from "../award/award.component";

@Component({
  selector: "cv-award-list",
  templateUrl: "./award-list.component.html",
  styleUrls: ["./award-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AwardListComponent {
  public awards: Award[] = [
    {
      competition: "Swiss Championship — Web Design",
      timeframe: "2014",
      placement: "4th"
    },
    {
      competition: "Swiss Championship — Web Design",
      timeframe: "2013",
      placement: "5th"
    },
  ];

  public trackByIndex(index: number, item: Award): number {
    return index;
  }
}
