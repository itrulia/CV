import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {AwardListComponent} from "./award-list.component";
import {ComponentsModule} from "../../components/components.module";
import {AwardComponent} from "../award/award.component";

describe("AwardListComponent", () => {
  let component: AwardListComponent;
  let fixture: ComponentFixture<AwardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AwardListComponent, AwardComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
