import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AwardComponent} from "./award/award.component";
import {AwardListComponent} from "./award-list/award-list.component";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    AwardComponent,
    AwardListComponent
  ],
  exports: [
    AwardComponent,
    AwardListComponent
  ]
})
export class AwardsModule {}
