import {Component, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-framework-list",
  templateUrl: "./framework-list.component.html",
  styleUrls: ["./framework-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FrameworkListComponent {}
