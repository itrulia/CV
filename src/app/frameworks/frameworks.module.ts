import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FrameworkListComponent} from "./framework-list/framework-list.component";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [FrameworkListComponent],
  exports: [FrameworkListComponent]
})
export class FrameworksModule { }
