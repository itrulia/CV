import {ExperiencesModule} from "./experiences.module";

describe("ExperiencesModule", () => {
  let experiencesModule: ExperiencesModule;

  beforeEach(() => {
    experiencesModule = new ExperiencesModule();
  });

  it("should create an instance", () => {
    expect(experiencesModule).toBeTruthy();
  });
});
