import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {EducationExperienceListComponent} from "./education-experience-list.component";
import {ComponentsModule} from "../../components/components.module";
import {ExperienceComponent} from "../experience/experience.component";

describe("EducationExperienceListComponent", () => {
  let component: EducationExperienceListComponent;
  let fixture: ComponentFixture<EducationExperienceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EducationExperienceListComponent, ExperienceComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationExperienceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
