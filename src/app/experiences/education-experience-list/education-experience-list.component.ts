import {Component, ChangeDetectionStrategy} from "@angular/core";
import {Experience} from "../experience/experience.component";

@Component({
  selector: "cv-education-experience-list",
  templateUrl: "./education-experience-list.component.html",
  styleUrls: ["./education-experience-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EducationExperienceListComponent {
  public experiences: Experience[] = [
    {
      location: "BBW – Winterthur, CH",
      job: "Vocational School",
      timeframe: "2011 – 2015"
    },
  ];

  public trackByIndex(index: number, item: Experience): number {
    return index;
  }
}
