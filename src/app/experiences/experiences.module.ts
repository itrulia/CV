import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {WorkingExperienceListComponent} from "./working-experience-list/working-experience-list.component";
import {EducationExperienceListComponent} from "./education-experience-list/education-experience-list.component";
import {ExperienceComponent} from "./experience/experience.component";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [WorkingExperienceListComponent, EducationExperienceListComponent, ExperienceComponent],
  exports: [WorkingExperienceListComponent, EducationExperienceListComponent, ExperienceComponent]
})
export class ExperiencesModule {}
