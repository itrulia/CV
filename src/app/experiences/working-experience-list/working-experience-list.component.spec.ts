import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {WorkingExperienceListComponent} from "./working-experience-list.component";
import {ComponentsModule} from "../../components/components.module";
import {ExperienceComponent} from "../experience/experience.component";

describe("WorkingExperienceListComponent", () => {
  let component: WorkingExperienceListComponent;
  let fixture: ComponentFixture<WorkingExperienceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkingExperienceListComponent, ExperienceComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingExperienceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
