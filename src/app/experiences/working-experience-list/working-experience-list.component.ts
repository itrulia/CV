import {Component, ChangeDetectionStrategy} from "@angular/core";
import {Experience} from "../experience/experience.component";

@Component({
  selector: "cv-working-experience-list",
  templateUrl: "./working-experience-list.component.html",
  styleUrls: ["./working-experience-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkingExperienceListComponent {

  public experiences: Experience[] = [
    {
      location: "Cultural Care Au Pair / Education First — Zürich, CH",
      job: "Frontend Developer",
      timeframe: "2019",
      ongoing: true
    },
    {
      location: "Turtle eSports Technology GmbH — Köln, DE",
      job: "(Senior/Lead) Frontend Developer",
      timeframe: "2015 – 2019",
    },
    {
      location: "AAC Infotray AG — Winterthur, CH",
      job: "Apprenticeship — Software Engineer",
      timeframe: "2011 – 2015"
    },
    {
      location: "SmashingMedia — Freiburg, DE",
      job: "Volunteer — SmashingConf Freiburg",
      timeframe: "2014"
    },
    {
      location: "Google — Web Starter Kit",
      job: "Volunteer — Frontend Developer",
      timeframe: "2014"
    },
    {
      location: "Tukui.org",
      job: "Volunteer — Software Engineer",
      timeframe: "2009 – 2011"
    },
  ];

  public trackByIndex(index: number, item: Experience): number {
    return index;
  }
}
