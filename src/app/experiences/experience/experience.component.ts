import {Component, Input, ChangeDetectionStrategy} from "@angular/core";

export interface Experience {
  timeframe: string;
  job: string;
  location: string;
  ongoing?: boolean;
}

@Component({
  selector: "cv-experience",
  templateUrl: "./experience.component.html",
  styleUrls: ["./experience.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceComponent {
  @Input()
  public timeframe: string;

  @Input()
  public location: string;

  @Input()
  public ongoing?: boolean;
}
