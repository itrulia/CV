import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CodeListComponent} from "./code-list/code-list.component";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [CodeListComponent],
  exports: [CodeListComponent]
})
export class CodeModule { }
