import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {CodeListComponent} from "./code-list.component";
import {ComponentsModule} from "../../components/components.module";

describe("CodeListComponent", () => {
  let component: CodeListComponent;
  let fixture: ComponentFixture<CodeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodeListComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
