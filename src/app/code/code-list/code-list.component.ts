import {Component, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-code-list",
  templateUrl: "./code-list.component.html",
  styleUrls: ["./code-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeListComponent {}
