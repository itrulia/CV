import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {LastjobComponent} from "./lastjob.component";
import {ComponentsModule} from "../../components/components.module";

describe("LastjobComponent", () => {
  let component: LastjobComponent;
  let fixture: ComponentFixture<LastjobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LastjobComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastjobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
