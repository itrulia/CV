import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {AboutmeComponent} from "./aboutme.component";
import {ComponentsModule} from "../../components/components.module";

describe("AboutmeComponent", () => {
  let component: AboutmeComponent;
  let fixture: ComponentFixture<AboutmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutmeComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
