import {Component, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-aboutme",
  templateUrl: "./aboutme.component.html",
  styleUrls: ["./aboutme.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutmeComponent {}
