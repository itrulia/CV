import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AboutmeComponent} from "./aboutme/aboutme.component";
import {ComponentsModule} from "../components/components.module";
import { LastjobComponent } from "./lastjob/lastjob.component";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [AboutmeComponent, LastjobComponent],
  exports: [AboutmeComponent, LastjobComponent]
})
export class AboutmeModule {}
