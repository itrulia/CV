import {TestBed, async} from "@angular/core/testing";
import {AppComponent} from "./app.component";
import {ExperiencesModule} from "./experiences/experiences.module";
import {AboutmeModule} from "./aboutme/aboutme.module";
import {FrameworksModule} from "./frameworks/frameworks.module";
import {ToolsModule} from "./tools/tools.module";
import {CodeModule} from "./code/code.module";
import {AwardsModule} from "./awards/awards.module";
import {LanguagesModule} from "./languages/languages.module";
import {ProfileComponent} from "./profile/profile.component";

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ProfileComponent
      ],
      imports: [
        ExperiencesModule,
        AboutmeModule,
        FrameworksModule,
        ToolsModule,
        CodeModule,
        AwardsModule,
        LanguagesModule
      ]
    }).compileComponents();
  }));

  it("should create the app", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
