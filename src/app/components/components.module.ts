import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ListTitleComponent} from "./list-title/list-title.component";
import {NamedItemComponent} from "./named-item/named-item.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ListTitleComponent, NamedItemComponent],
  exports: [ListTitleComponent, NamedItemComponent]
})
export class ComponentsModule { }
