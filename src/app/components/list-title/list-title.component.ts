import {Component, Input, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-list-title",
  templateUrl: "./list-title.component.html",
  styleUrls: ["./list-title.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListTitleComponent {
  @Input()
  public icon: string;
}
