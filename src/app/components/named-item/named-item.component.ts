import {Component, Input, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-named-item",
  templateUrl: "./named-item.component.html",
  styleUrls: ["./named-item.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NamedItemComponent {
  @Input()
  public name: string;
}
