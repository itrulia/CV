import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TechnologiesComponent } from './technologies/technologies.component';
import {ComponentsModule} from '../components/components.module';



@NgModule({
  declarations: [TechnologiesComponent],
  exports: [TechnologiesComponent],
  imports: [
    CommonModule,
    ComponentsModule
  ]
})
export class TechnologiesModule { }
