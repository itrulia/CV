import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ToolListComponent} from "./tool-list/tool-list.component";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [ToolListComponent],
  exports: [ToolListComponent]
})
export class ToolsModule { }
