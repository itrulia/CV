import {Component, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-tool-list",
  templateUrl: "./tool-list.component.html",
  styleUrls: ["./tool-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolListComponent {}
