import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppComponent} from "./app.component";
import {ExperiencesModule} from "./experiences/experiences.module";
import {AboutmeModule} from "./aboutme/aboutme.module";
import {FrameworksModule} from "./frameworks/frameworks.module";
import {ToolsModule} from "./tools/tools.module";
import {CodeModule} from "./code/code.module";
import {AwardsModule} from "./awards/awards.module";
import { ProfileComponent } from "./profile/profile.component";
import {LanguagesModule} from "./languages/languages.module";
import {TechnologiesModule} from "./technologies/technologies.module";

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    ExperiencesModule,
    AboutmeModule,
    FrameworksModule,
    ToolsModule,
    CodeModule,
    AwardsModule,
    LanguagesModule,
    TechnologiesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
