import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {LanguageComponent} from "./language.component";
import {ComponentsModule} from "../../components/components.module";

describe("LanguageComponent", () => {
  let component: LanguageComponent;
  let fixture: ComponentFixture<LanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LanguageComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
