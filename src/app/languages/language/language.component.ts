import {Component, ChangeDetectionStrategy, Input} from "@angular/core";

@Component({
  selector: "cv-language",
  templateUrl: "./language.component.html",
  styleUrls: ["./language.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LanguageComponent {
  @Input()
  public language: string;

  @Input()
  public level: string;
}
