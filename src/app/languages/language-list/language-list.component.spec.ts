import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {LanguageListComponent} from "./language-list.component";
import {ComponentsModule} from "../../components/components.module";
import {LanguageComponent} from "../language/language.component";

describe("LanguageListComponent", () => {
  let component: LanguageListComponent;
  let fixture: ComponentFixture<LanguageListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LanguageListComponent, LanguageComponent],
      imports: [ComponentsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
