import {Component, ChangeDetectionStrategy} from "@angular/core";

@Component({
  selector: "cv-language-list",
  templateUrl: "./language-list.component.html",
  styleUrls: ["./language-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LanguageListComponent {}
