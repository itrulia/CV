import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LanguageListComponent} from "./language-list/language-list.component";
import {LanguageComponent} from "./language/language.component";
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [LanguageListComponent, LanguageComponent],
  exports: [LanguageListComponent, LanguageComponent]
})
export class LanguagesModule { }
