# Karl Merkli's Curriculum Vitae
> Written in Angular

Hey guys :) This is my personal CV written in Angular, this is a rewrite of my old CV that was still written in Angularjs and was over 5 years old from now on.

This repository doesn't reflect every best practise you can find in the internet, with the following reasons

* Due to time restrictions, implementing state wasn't needed
* Due to this being mostly a "private" repository, it does not contain documentation, it's also fairly simple and I am more than willing to help you navigate through this application if you have any trouble :)
* Due to this not being a web document in the end, but a printed out file, it does not include accessibility best practises
* Due to this not being a web document in the end, but a printed out file, it may not work in every browser
* Due to not wanting to make it too complicated, there might be some code duplications.


## How to run it

Dependencies:

* Node 9+ (Probably also works with older versions)
* A browser

First you need to download it from this git repository so that you can run it on your PC, you can either do that with a git client or download it straight away from gitlab.com.

After this, you will need to install the npm dependencies, you can do that by running `npm install` inside the project root folder. You can now start the @angular/cli development server by executing `npx ng serve` and view it by navigating to `http://localhost:4200/` in your browser.

### How to run tests

You can execute the tests via `npx ng test` or `npx ng test --browsers ChromeHeadless` if you want to run it without it opening a visible browser (headless).


## Copyright notice

This repository repackages SVG images, I do not own any rights to those images apart from the photo I took of me. Out of simplicity most logos were downloaded from https://worldvectorlogo.com/.

If you are one of the copyright holders of those SVG images and wan't me to credit your work directly in this repository, please reach out to me at <karlmerkli@gmail.com> and I will be very happy to conform to your needs.